from django.urls import path
from . import views

urlpatterns = [
    path('', views.ContactListView.as_view(), name='contacts'),
    path('delete/<str:id>/', views.deleteContactView, name='delete'),
    path('newtype/', views.addType, name='newtype'),
    path('newsub/', views.addSubject, name='newsub'),
    path('edit/', views.editContactView, name='edit')
]