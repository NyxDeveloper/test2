from django import forms
from django.forms import ModelForm
from .models import Contact
from .models import Type
from .models import Subject


class AddContactForm(ModelForm):
    class Meta:
        model = Contact
        fields = ['info', 'type', 'subject']
        widgets = {
            'info': forms.TextInput(attrs={
                'placeholder': 'Данные контакта',
                'class': 'form-control h-100 w-100'
            }),
            'type': forms.Select(attrs={
                'class': 'form-control h-100 w-100'
            }),
            'subject': forms.Select(attrs={
                'class': 'form-control h-100 w-100'
            })
        }


class EditContactForm(ModelForm):
    class Meta:
        model = Contact
        fields = ['KEY', 'info', 'type', 'subject']
        widgets = {
            'KEY': forms.NumberInput(attrs={
                'placeholder': 'ID',
                'class': 'form-control w-25'
            }),
            'info': forms.TextInput(attrs={
                'placeholder': 'Данные контакта',
                'class': 'form-control w-25'
            }),
            'type': forms.Select(attrs={
                'class': 'form-control w-25'
            }),
            'subject': forms.Select(attrs={
                'class': 'form-control w-25'
            })
        }

    def __init__(self, *args, **kwargs):
        super(EditContactForm, self).__init__(*args, **kwargs)
        self.fields['info'].required = False
        self.fields['type'].required = False
        self.fields['subject'].required = False


class AddTypeForm(ModelForm):
    class Meta:
        model = Type
        fields = ['name']
        widgets = {
            'name': forms.TextInput(attrs={
                'placeholder': 'Новый тип',
                'class': 'form-control h-100 w-100'
            })
        }


class AddSubjectForm(ModelForm):
    class Meta:
        model = Subject
        fields = ['name']
        widgets = {
            'name': forms.TextInput(attrs={
                'placeholder': 'Новый человек',
                'class': 'form-control h-100 w-100'
            })
        }


class SearchContactForm(ModelForm):
    class Meta:
        model = Contact
        fields = ['subject', 'type', 'info', 'date']
        widgets = {
            'info': forms.TextInput(attrs={
                'placeholder': 'Данные контакта',
                'class': 'form-control w-25 m-1'
            }),
            'type': forms.Select(attrs={
                'class': 'form-control w-25 m-1'
            }),
            'subject': forms.Select(attrs={
                'class': 'form-control w-25 m-1'
            }),
            'date': forms.DateInput(attrs={
                'class': 'form-control m-1'
            })
        }

    def __init__(self, *args, **kwargs):
        super(SearchContactForm, self).__init__(*args, **kwargs)
        self.fields['info'].required = False
        self.fields['type'].required = False
        self.fields['subject'].required = False
        self.fields['date'].required = False


class AjaxSearchIdForm(ModelForm):
    class Meta:
        model = Contact
        fields = ['KEY']
        widgets = {
            'KEY': forms.TextInput(attrs={
                'placeholder': 'ID',
                'class': 'form-control w-100 h100',
                'id': 'ajax_search_id_form',
                'value': ''
            })
        }

    def __init__(self, *args, **kwargs):
        super(AjaxSearchIdForm, self).__init__(*args, **kwargs)
        self.fields['KEY'].required = False


class AjaxSearchSubjectForm(ModelForm):
    class Meta:
        model = Contact
        fields = ['subject']
        widgets = {
            'subject': forms.TextInput(attrs={
                'placeholder': 'Имя',
                'class': 'form-control w-100 h100'
            })
        }

    def __init__(self, *args, **kwargs):
        super(AjaxSearchSubjectForm, self).__init__(*args, **kwargs)
        self.fields['subject'].required = False


class AjaxSearchTypeForm(ModelForm):
    class Meta:
        model = Contact
        fields = ['type']
        widgets = {
            'type': forms.TextInput(attrs={
                'placeholder': 'Тип',
                'class': 'form-control w-100 h100'
            })
        }

    def __init__(self, *args, **kwargs):
        super(AjaxSearchTypeForm, self).__init__(*args, **kwargs)
        self.fields['type'].required = False


class AjaxSearchInfoForm(ModelForm):
    class Meta:
        model = Contact
        fields = ['info']
        widgets = {
            'info': forms.TextInput(attrs={
                'placeholder': 'Инфо',
                'class': 'form-control w-100 h100'
            })
        }

    def __init__(self, *args, **kwargs):
        super(AjaxSearchInfoForm, self).__init__(*args, **kwargs)
        self.fields['info'].required = False


class AjaxSearchDateForm(ModelForm):
    class Meta:
        model = Contact
        fields = ['date']
        widgets = {
            'date': forms.TextInput(attrs={
                'placeholder': 'Дата',
                'class': 'form-control w-100 h100'
            })
        }
