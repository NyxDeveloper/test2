from django.db import models
import datetime


class Type(models.Model):
    name = models.CharField('Название', max_length=50)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name


class Subject(models.Model):
    name = models.CharField('Имя', max_length=50)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name


class Contact(models.Model):
    KEY = models.IntegerField('Идентификатор', default=None, null=True, blank=True)
    info = models.CharField('Инфо', max_length=100)
    date = models.DateField(default=datetime.date.today(), verbose_name='Дата добавления')
    type = models.ForeignKey(Type, verbose_name='Тип', on_delete=models.CASCADE, default=None)
    subject = models.ForeignKey(Subject, verbose_name='Человек', on_delete=models.CASCADE, default=None)

    class Meta:
        ordering = ['KEY', 'info', 'type__name', 'subject__name', 'date']

    def __str__(self):
        return self.info


class ContactCache(models.Model):
    user_id = models.IntegerField(unique=True)

    KEY = models.CharField(max_length=100, default='', blank=True)
    info = models.CharField(max_length=100, default='', blank=True)
    type = models.CharField(max_length=100, default='', blank=True)
    subject = models.CharField(max_length=100, default='', blank=True)


class ContactContext(models.Model):
    KEY = models.CharField(max_length=100, default='', blank=True)
    info = models.CharField(max_length=100, default='', blank=True)
    type = models.CharField(max_length=100, default='', blank=True)
    subject = models.CharField(max_length=100, default='', blank=True)
