from django.contrib import admin
from .models import Contact
from .models import Subject
from .models import Type
from .models import ContactCache
from .models import ContactContext


admin.site.register(Contact)
admin.site.register(Subject)
admin.site.register(Type)
admin.site.register(ContactCache)
admin.site.register(ContactContext)