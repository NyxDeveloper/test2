import datetime
import json

from django.core.paginator import Paginator
from django.core.paginator import EmptyPage
from django.core.paginator import PageNotAnInteger
from django.http import HttpResponseRedirect, HttpResponse, HttpResponseBadRequest, JsonResponse
from django.http import HttpResponseNotFound
from django.views.generic import FormView, UpdateView

from django.shortcuts import render
from django.views import View

from .models import Contact
from .models import Type
from .models import Subject
from .models import ContactCache
from .models import ContactContext

from .forms import AddContactForm
from .forms import EditContactForm
from .forms import AddTypeForm
from .forms import AddSubjectForm

from .forms import SearchContactForm

from .forms import AjaxSearchIdForm
from .forms import AjaxSearchSubjectForm
from .forms import AjaxSearchTypeForm
from .forms import AjaxSearchInfoForm
from .forms import AjaxSearchDateForm


class ContactListView(View):
    def get(self, request):

        contacts = Contact.objects.all()

        #   объект кэша
        cache, created = ContactCache.objects.get_or_create(user_id=request.user.id)
        #   фильтруем по кэшу
        if cache.KEY and cache.KEY is not '' or request.GET.get('KEY') and request.GET.get('KEY') is not '' and request.GET.get('KEY') is not None:
            if cache.KEY and cache.KEY is not '':
                contacts = contacts.filter(id=cache.KEY)
            if request.GET.get('KEY') and request.GET.get('KEY') is not '':
                contacts = contacts.filter(id=request.GET.get('KEY'))
                cache.KEY = request.GET.get('KEY')
                cache.save()

        if cache.subject and cache.subject is not '' or request.GET.get('subject') and request.GET.get('subject') is not '' and request.GET.get('subject') is not None:
            if cache.subject and cache.subject is not '':
                contacts = contacts.filter(subject__name=cache.subject)
            if request.GET.get('subject') and request.GET.get('subject') is not '':
                contacts = contacts.filter(subject__name=request.GET.get('subject'))
                cache.subject = request.GET.get('subject')
                cache.save()

        if cache.type and cache.type is not '' or request.GET.get('type') and request.GET.get('type') is not '' and request.GET.get('type') is not None:
            if cache.type and cache.type is not '':
                contacts = contacts.filter(type__name=cache.type)
            if request.GET.get('type') and request.GET.get('type') is not '':
                contacts = contacts.filter(type__name=request.GET.get('type'))
                cache.type = request.GET.get('type')
                cache.save()

        if cache.info and cache.info is not '' or request.GET.get('info') and request.GET.get('info') is not '' and request.GET.get('info') is not None:
            if cache.info and cache.info is not '':
                contacts = contacts.filter(info=cache.info)
            if request.GET.get('info') and request.GET.get('info') is not '':
                contacts = contacts.filter(info=request.GET.get('info'))
                cache.info = request.GET.get('info')
                cache.save()

        #   очистка кэша
        if request.GET.get('KEY') is '':
            cache.KEY = ''
            cache.save()

        if request.GET.get('subject') is '':
            cache.subject = ''
            cache.save()

        if request.GET.get('type') is '':
            cache.type = ''
            cache.save()

        if request.GET.get('info') is '':
            cache.info = ''
            cache.save()

        #   создание контекста
        context = ContactContext()
        if cache.KEY and cache.KEY is not '' or request.GET.get('KEY') and request.GET.get('KEY') is not '' and request.GET.get('KEY') is not None:
            if cache.KEY and cache.KEY is not '':
                context.KEY = cache.KEY
                context.save()
            if request.GET.get('KEY') and request.GET.get('KEY') is not '' and request.GET.get('KEY') is not None:
                context.KEY = request.GET.get('KEY')
                context.save()

        if cache.subject and cache.subject is not '' or request.GET.get('subject') and request.GET.get('subject') is not '' and request.GET.get('subject') is not None:
            if cache.subject and cache.subject is not '':
                context.subject = cache.subject
                context.save()
            if request.GET.get('subject') and request.GET.get('subject') is not '' and request.GET.get('subject') is not None:
                context.subject = request.GET.get('subject')
                context.save()

        if cache.type and cache.type is not '' or request.GET.get('type') and request.GET.get('type') is not '' and request.GET.get('type') is not None:
            if cache.type and cache.type is not '':
                context.type = cache.type
                context.save()
            if request.GET.get('type') and request.GET.get('type') is not '' and request.GET.get('type') is not None:
                context.type = request.GET.get('type')
                context.save()

        if cache.info and cache.info is not '' or request.GET.get('info') and request.GET.get('info') is not '' and request.GET.get('info') is not None:
            if cache.info and cache.info is not '':
                context.info = cache.info
                context.save()
            if request.GET.get('info') and request.GET.get('info') is not '' and request.GET.get('info') is not None:
                context.info = request.GET.get('info')
                context.save()

        #   очистка контекста
        if request.GET.get('KEY') is '':
            context.KEY = ''
            context.save()

        if request.GET.get('subject') is '':
            context.subject = ''
            context.save()

        if request.GET.get('type') is '':
            context.type = ''
            context.save()

        if request.GET.get('info') is '':
            context.info = ''
            context.save()

        #   сортировка
        KEYSorted = False
        subjectSorted = False
        typeSorted = False
        infoSorted = False
        dateSorted = False

        #   по возрастанию
        if 'KEY_sort1' in request.GET:
            contacts = contacts.order_by('KEY')
            KEYSorted = True
        if 'subject_sort1' in request.GET:
            contacts = contacts.order_by('subject')
            subjectSorted = True
        if 'type_sort1' in request.GET:
            contacts = contacts.order_by('type')
            typeSorted = True
        if 'info_sort1' in request.GET:
            contacts = contacts.order_by('info')
            infoSorted = True
        if 'date_sort1' in request.GET:
            contacts = contacts.order_by('date')
            dateSorted = True

        #   по убыванию
        if 'KEY_sort2' in request.GET:
            contacts = contacts.order_by('-KEY')
            KEYSorted = False
        if 'subject_sort2' in request.GET:
            contacts = contacts.order_by('-subject')
            subjectSorted = False
        if 'type_sort2' in request.GET:
            contacts = contacts.order_by('-type')
            typeSorted = False
        if 'info_sort2' in request.GET:
            contacts = contacts.order_by('-info')
            infoSorted = False
        if 'date_sort2' in request.GET:
            contacts = contacts.order_by('-date')
            dateSorted = False



        paginator = Paginator(contacts, 4)
        page = request.GET.get('page')

        try:
            contacts = paginator.page(page)
        except PageNotAnInteger:
            contacts = paginator.page(1)
        except EmptyPage:
            contacts = paginator.page(paginator.num_pages)

        # if 'page' in request.GET:
        #     page = request.GET.get('page', 1)
        #     contacts = paginator.page(page)

        addContact = AddContactForm
        editContact = EditContactForm
        addType = AddTypeForm
        addSubject = AddSubjectForm

        ajaxIdSearch = AjaxSearchIdForm
        ajaxTypeSearch = AjaxSearchTypeForm
        ajaxSubjectSearch = AjaxSearchSubjectForm
        ajaxInfoSearch = AjaxSearchInfoForm
        ajaxDateSearch = AjaxSearchDateForm

        search = SearchContactForm



        return render(request, 'contacts.html', {
            'contacts': contacts,
            'addContact': addContact,
            'addType': addType,
            'addSubject': addSubject,
            'editContact': editContact,
            'search': search,
            'ajaxIdSearch': ajaxIdSearch,
            'ajaxTypeSearch': ajaxTypeSearch,
            'ajaxSubjectSearch': ajaxSubjectSearch,
            'ajaxInfoSearch': ajaxInfoSearch,
            'ajaxDateSearch': ajaxDateSearch,
            'cache': cache,
            'context': context,
            'KEYSorted': KEYSorted,
            'subjectSorted': subjectSorted,
            'typeSorted': typeSorted,
            'infoSorted': infoSorted,
            'dateSorted': dateSorted
        })

    def post(self, request):
        if request.method == 'POST':
            form = AddContactForm(request.POST)
            if form.is_valid():
                if request.POST['indicator'] == '4':
                    # сохранение формы
                    contact = Contact()  # новый объект
                    contact.type = form.cleaned_data['type']
                    contact.subject = form.cleaned_data['subject']
                    contact.info = form.cleaned_data['info']
                    contact.date = datetime.date.today()
                    contact.KEY = contact.id
                    contact.save()
                    return HttpResponseRedirect('/contacts/')

            form = SearchContactForm(request.POST)
            if form.is_valid():
                if request.POST['indicator'] == '1':
                    contacts = Contact.objects.all()
                    if 'subject' in request.POST and request.POST['subject'] is not '':
                        contacts = contacts.filter(subject__id=request.POST['subject'])
                    if 'type' in request.POST and request.POST['type'] is not '':
                        contacts = contacts.filter(type__id=request.POST['type'])
                    if 'info' in request.POST and request.POST['info'] is not '':
                        contacts = contacts.filter(info=request.POST['info'])
                    if 'date' in request.POST and request.POST['date'] is not '':
                        contacts = contacts.filter(date=form.cleaned_data['date'])

                    paginator = Paginator(contacts, 4)
                    page = request.GET.get('page')

                    try:
                        contacts = paginator.page(page)
                    except PageNotAnInteger:
                        contacts = paginator.page(1)
                    except EmptyPage:
                        contacts = paginator.page(paginator.num_pages)

                    # if 'page' in request.GET:
                    #     page = request.GET.get('page', 1)
                    #     contacts = paginator.page(page)

                    addContact = AddContactForm
                    editContact = EditContactForm
                    addType = AddTypeForm
                    addSubject = AddSubjectForm

                    ajaxIdSearch = AjaxSearchIdForm
                    ajaxTypeSearch = AjaxSearchTypeForm
                    ajaxSubjectSearch = AjaxSearchSubjectForm
                    ajaxInfoSearch = AjaxSearchInfoForm
                    ajaxDateSearch = AjaxSearchDateForm

                    search = SearchContactForm

                    return render(request, 'contacts.html', {
                        'contacts': contacts,
                        'addContact': addContact,
                        'addType': addType,
                        'addSubject': addSubject,
                        'editContact': editContact,
                        'search': search,
                        'ajaxIdSearch': ajaxIdSearch,
                        'ajaxTypeSearch': ajaxTypeSearch,
                        'ajaxSubjectSearch': ajaxSubjectSearch,
                        'ajaxInfoSearch': ajaxInfoSearch,
                        'ajaxDateSearch': ajaxDateSearch
                    })

                return HttpResponseRedirect('/contacts/')


def editContactView(request):
    if request.method == 'POST':
        form = EditContactForm(request.POST)
        if form.is_valid():
            old = Contact.objects.get(id=form.cleaned_data['KEY'])
            new = Contact()
            if form.cleaned_data['type']:
                new.type = form.cleaned_data['type']
            else:
                new.type = old.type
            if form.cleaned_data['subject']:
                new.subject = form.cleaned_data['subject']
            else:
                new.subject = old.subject
            if form.cleaned_data['info']:
                new.info = form.cleaned_data['info']
            else:
                new.info = old.info

            new.id = old.id
            old.delete()
            new.save()

            return HttpResponseRedirect('/contacts/')
    return HttpResponseRedirect('/contacts/')


def deleteContactView(request, id):
    try:
        contact = Contact.objects.get(id=id)
        contact.delete()
        return HttpResponseRedirect('/contacts')
    except Contact.DoesNotExist:
        return HttpResponseNotFound('Contact not exist')


def addType(request):
    if request.method == 'POST':
        form = AddTypeForm(request.POST)
        if form.is_valid():
            type = Type()
            type.name = form.cleaned_data['name']
            type.save()
            return HttpResponseRedirect('/contacts/')


def addSubject(request):
    if request.method == 'POST':
        form = AddTypeForm(request.POST)
        if form.is_valid():
            sub = Subject()
            sub.name = form.cleaned_data['name']
            sub.save()
            return HttpResponseRedirect('/contacts/')

