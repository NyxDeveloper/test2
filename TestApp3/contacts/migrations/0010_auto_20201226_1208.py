# Generated by Django 2.2 on 2020-12-26 12:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contacts', '0009_auto_20201226_1201'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contactcache',
            name='user_id',
            field=models.IntegerField(blank=True, null=True, unique=True),
        ),
    ]
