# Generated by Django 2.2 on 2020-12-24 07:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contacts', '0003_auto_20201224_0626'),
    ]

    operations = [
        migrations.AddField(
            model_name='contact',
            name='KEY',
            field=models.IntegerField(blank=True, default=None, null=True, verbose_name='Идентификатор'),
        ),
    ]
